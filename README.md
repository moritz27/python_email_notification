# Send Email Notifications from Python 

## Email Configuration

```json
{
  "username": "your@mail.com",
  "password": "yourPasssword",
}
```

## Receiver(s) Configuration

```json
{
    "Name_1": "E@mail_1.com",
    "Name_2": "E@mail_2.com",
    ...
}
```