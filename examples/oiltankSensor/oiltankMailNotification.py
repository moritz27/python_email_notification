#!/usr/bin/python

# run "pm2 start ecosystem.config.json" from this folder to start sending emails

import sys
import os

root_folder = os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
sys.path.append(root_folder)

from mailNotificator import MAIL_NOTIFACTOR
from influxOiltankQuery import queryOiltankLevel

try:
    script_dir = os.path.dirname(os.path.realpath(__file__))
    receiversFilePath = os.path.join(script_dir, "receivers.json")
    mail_notificator = MAIL_NOTIFACTOR(receiversFilePath)
    mail_notificator.sendAllEmails("Füllstand Heizöltank", queryOiltankLevel(), "Hallo", "Die nächste Info Mail wird Montag um 8 Uhr gesendet.")
except Exception as e:
    print("Error occured while sending Email:", e)
    sys.exit(1)
finally:
    mail_notificator.account.quit()



