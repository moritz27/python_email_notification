from influxdb_client import InfluxDBClient
import os

def queryOiltankLevel():
    root_folder = os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
    path = os.path.join(root_folder, "credentials", "influxDB2.json")
    print(path)
    client = InfluxDBClient.from_config_file(path)
    query_api = client.query_api()
    query = ''' 
        value_now = from(bucket: "Sensors")
        |> range(start: -1h)
        |> filter(fn: (r) => 
            r["_measurement"] == "Home" and
            r["_field"] == "Distance" and
            r["Location"] == "Oiltank"
            )
        |> median()
        |> map(fn: (r) => ({r with _value: 1.5 - r._value}))


        value_last_week = from(bucket: "Sensors")
        |> range(start: -31d, stop: -30d)
        |> filter(fn: (r) => 
            r["_measurement"] == "Home" and
            r["_field"] == "Distance" and
            r["Location"] == "Oiltank"
            )
        |> median()
        |> map(fn: (r) => ({r with _value: 1.5 - r._value}))

        union(tables: [value_now, value_last_week])
        |> pivot(rowKey: ["_start"], columnKey: ["_field"], valueColumn: "_value")
        '''

    results = query_api.query_data_frame(query)

    client.close()

    usedOil = str(int(results["Distance"][0] * 1000 -
                results["Distance"][1] * 1000))

    infoString = "der aktuelle Füllstand des Heizöls beträgt " + \
        str(int(results["Distance"][1] * 1000)) + \
        "mm.\nIn den letzen 30 Tagen wurden ca. " + usedOil + "mm verbraucht."

    return infoString