#!/usr/bin/python

import smtplib
import os
import json
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class MAIL_NOTIFACTOR():
    def __init__(self, receiversFilePath):
        script_dir = os.path.dirname(os.path.realpath(__file__))
        mailConfigPath = os.path.join(script_dir, "credentials", "mail.json")
        self.mailConfig = self.readJsonFile(mailConfigPath)
        self.account = self.setupEmail(self.mailConfig)
        self.receivers = self.readJsonFile(receiversFilePath)

    def readJsonFile(self, path):
        ''' Returns the parsed content of a specified path to a JSON file'''
        try:
            print("reading json from:", path)
            if os.path.exists(path):
                f = open(path, "r")
                data = json.loads(f.read())
                f.close()
                return data
            else:
                return 1
        except:
            return 1

    def setupEmail(self, mailConfig):
        account = smtplib.SMTP("smtp.gmail.com", 587)
        account.starttls()
        account.login(mailConfig["username"], mailConfig["password"])
        return account

    def buildEmailText(self, name, emailContent, preText="Hello", afterText=""):
        return (preText + " " + name + ",\n" + emailContent + "\n" + afterText)

    def sendEmail(self, receiverMail, emailSubject, emailText):
        msg = MIMEMultipart()
        msg['From'] = self.mailConfig["username"]
        msg['To'] = receiverMail
        msg['Subject'] = emailSubject
        msg.attach(MIMEText(emailText, 'plain'))
        self.account.send_message(msg)
        print("Successfully sent E-Mail to:", receiverMail)

    def sendAllEmails(self, subject, content, preText="Hello", afterText=""):
        for name in self.receivers:
            emailText = self.buildEmailText(name, content, preText, afterText)
            self.sendEmail(self.receivers[name], subject, emailText)
